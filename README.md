# Digital Brief

|   |   |   |   |
|---|---|---|---|
| **Client** | [SLG Agency](http://slg.agency) | **Date** | 08/06/2018 |
| **Job Name** | Developer Technical Test | **Estimated completion time** | 8 hours |
| **Job Number** | SLG-18-XXX | | |

##The Background
*What is the challenge? What do we need to understand the context of the brief?*

SLG Agency are a full service marketing agency in Manchester’s Northern Quarter and have an upcoming email marketing campaign that requires a HTML email and corresponding destination landing page creating for visitors to register their interest.

##The Task
Using the provided PDF and art assets as a guide you are to create a HTML email for mass distribution and a standalone landing page to complement our upcoming email marketing campaign.

There is has been no decision made on the hosting technology or server location for the landing page so please feel free to use which ever technologies you feel is appropriate in the creation of this page. The copy has not yet been confirmed so use lorem ipsum where appropriate.

Return the completed page and email template to SLG via a repository on either [BitBucket](https://bitbucket.org/slgmarketingdev) or [GitHub](https://github.com/slgagency) alongside any deployment instructions you deem necessary.

 1. The landing page should be viewable across a range of device and browsers (IE9+, iOS, mobile Chrome, etc)
 2. The email must be viewable on most of the popular email clients including Gmail (mobile and desktop), Outlook 2016 and Thunderbird.
 3. Leave the email imagery as relative paths. We will amend these to work on our email marketing CDN when we test your work.
 
 You can get the official brief [here](test_brief.docx)